# README #

Voici une courte description du projet

### Environnement technique ###

* IntelliJ Idea
* Java 1.8
* Librairies uilisées : JUnit

### Problématique ###

L'idée est de d'ecrire un programme Java premettant de modéliser le jeu du "Pierre feuille Ciseau".

### Approche et choix  ###

Ce jeu a des règles de gestion basé sur une relation d'ordre entre les choix donc ce qui a poussé à  definir la notion de *GameComponent* (au lieu d'une énumération) qui représente les choix que nous pouvons faire dans le jeu.
Pour exprimer la relation d'ordre entre ces élements, j'ai opté pour une implémentation de l'interface Comparable afin de pouvoir donner une décision suite aux choix de deux joueurs.


Ainsi j'ai mis en place un test pour garantir les règles de la relation d'ordre qui sont les bases du jeu avant de passer à l'implémentation du code nécessaire (factory, programme main ..);

Des tests sont également écrits pour garantir le fait qu'au sortir du premier jeu le champ représentant le componentGame ne sera plus nul.

Pour bien contraindre nos *gameComponent* à respecter les contraintes d'intégrité pour une relation d'ordre cohérente, une notion de domination entre elles de sorte à renseigner le dominant dans le jeu de chacun lors de sa création.
Une exception sera levée dès qu'un objet reçoit un *dominant* qui n'est pas censé l'être.

Pour la fabrication de nos objets *gameComponent* le design Pattern *Singleton* pour ne créer qu'une instance des ces objets aux propriètés immuable dans le temps.
L'assignation du *dominant* à chaque objet sera effectué au fur et à mesure que ces objets sont construits , ainsi nous évitons un problème de dépendances cycliques entre objets pouvant causer une exception pour dépassement de mémoire ( succession de "new" à l'infini ). 

### Exécution du program  ###
Il suffira de cloner le projet et de l'importer sur IntelliJ Idea et de faire *run with* (de manière à voir le taux de couverture des tests) les etapes de compilation test packaging vont se faire successivement pour finir par le lancement du jar. 