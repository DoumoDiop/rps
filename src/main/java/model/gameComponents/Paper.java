package model.gameComponents;

import exception.UnAppropriateDominatorException;

/**
 * Class modeling a paper in the game
 */
public class Paper extends GameComponent {

    /**
     * @param _dominator
     */
    @Override
    public GameComponent setDominator(GameComponent _dominator) throws UnAppropriateDominatorException {
        if (_dominator instanceof Scissor)
        {
            dominator = _dominator;
            return this;
        }
        else
            throw new UnAppropriateDominatorException(this, _dominator);

    }

    @Override
    public String toString() {
        return "Paper";
    }

}
