package model.gameComponents;

import exception.UnAppropriateDominatorException;

/**
 *
 */
public abstract class GameComponent implements Comparable<GameComponent> {


    protected GameComponent dominator;

    /**
     * Sets the component which is upon the current one
     *
     * @param _dominator
     * @throws UnAppropriateDominatorException
     */
    public abstract GameComponent setDominator(GameComponent _dominator) throws UnAppropriateDominatorException;


    /**
     *
     * @param o
     * @return 0 : same game played , 1 ; current gameComponent is dominator of the one passed , -1 : current gameComponent is dominated of the one passed
     */
    public int compareTo(GameComponent o) {

        if( dominator.getClass().getSimpleName().equals(o.getClass().getSimpleName()))
            return -1;

        if( o.dominator.getClass().getSimpleName().equals(this.getClass().getSimpleName()))
            return 1;

        return 0;
    }
}
