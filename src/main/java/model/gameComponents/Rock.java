package model.gameComponents;

import exception.UnAppropriateDominatorException;

/**
 * Class modeling a rock in the game
 */
public class Rock extends GameComponent {
    /**
     * set the
     * @param _dominator
     */
    @Override
    public GameComponent setDominator(GameComponent _dominator) throws UnAppropriateDominatorException{
        if (_dominator instanceof Paper)
        {
            dominator = _dominator;
            return this;
        }
        else
            throw new UnAppropriateDominatorException(this, _dominator);
    }

    @Override
    public String toString() {
        return "Rock";
    }


}
