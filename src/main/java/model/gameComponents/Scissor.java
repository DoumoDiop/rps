package model.gameComponents;


import exception.UnAppropriateDominatorException;

/**
 * Class modeling a scissor in the game
 */
public class Scissor extends GameComponent {
    /**
     * @param _dominator
     */
    @Override
    public GameComponent setDominator(GameComponent _dominator) throws UnAppropriateDominatorException {
        if (_dominator instanceof Rock) {
            dominator = _dominator;
            return this;
        }
        else
            throw new UnAppropriateDominatorException(this, _dominator);
    }

    @Override
    public String toString() {
        return "Scissor";
    }


}
