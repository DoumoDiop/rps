package game;

import exception.UnAppropriateDominatorException;
import factory.gameFactory.GameFactory;
import factory.gameFactory.PlayerFactory;

import java.util.Scanner;

public class GameLauncher {


    public static void main(String [] args) throws  UnAppropriateDominatorException {
        Scanner scanner = new Scanner(System.in);
        System.out.println("SHI FU MI Game !!! Selection : ");

        int choice = 0;

        while (choice !=1 && choice !=2) {
            System.out.println("Type 1 : Human vs PC ;  Type 2 : PC vs PC ");
            choice = scanner.nextInt();

        }
        Game game ;
        if (choice == 1){
            game = GameFactory.createGame(PlayerFactory.createPlayerHuman("Human"), PlayerFactory.createPlayerComputer("Computer"));
        }
        else
        {
            game = GameFactory.createGame(PlayerFactory.createPlayerComputer("Computer 1"), PlayerFactory.createPlayerComputer("Computer 2"));

        }

        game.run();


    }
}
