package game;

import exception.UnAppropriateDominatorException;
import factory.componentsFactory.ComponentsFactory;
import model.gameComponents.GameComponent;
import player.PlayerAbs;

import java.util.HashMap;

/**
 * Class handling a whole game
 */
public class Game {
    private PlayerAbs p1;
    
    private PlayerAbs p2;

    public HashMap<String, GameComponent> getGameComponents() {
        return gameComponents;
    }

    private HashMap<String, GameComponent> gameComponents;

    public Game(PlayerAbs _p1, PlayerAbs _p2) throws UnAppropriateDominatorException {
        p1 = _p1;
        p2 = _p2;

        gameComponents = new HashMap<String, GameComponent>();

        gameComponents.put("R", ComponentsFactory.createRock().setDominator(ComponentsFactory.createPaper()));
        gameComponents.put("P", ComponentsFactory.createPaper().setDominator(ComponentsFactory.createScissor()));
        gameComponents.put("S", ComponentsFactory.createScissor().setDominator(ComponentsFactory.createRock()));
    }

    public void run() {

        int comparisonResult;
        while (p1.isWinner() == false && p2.isWinner() == false) {

            p1.play(gameComponents);
            p2.play(gameComponents);

            showResult();

            comparisonResult = p1.getComponentPlayed().compareTo(p2.getComponentPlayed());

            if (comparisonResult < 0) {
                p2.setWinner(true);
            } else if (comparisonResult >= 1)
                p1.setWinner(true);
        }


        if (p1.isWinner() == true)

        {
            System.out.println(p1.toString() + " wins ");
        } else

        {
            System.out.println(p2.toString() + " wins ");
        }


    }

    public PlayerAbs getP1() {
        return p1;
    }

    public PlayerAbs getP2() {
        return p2;
    }

    private void showResult() {
        p1.showPlayed();
        p2.showPlayed();
    }
}
