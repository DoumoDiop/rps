package player;

import model.gameComponents.GameComponent;

import java.util.HashMap;
import java.util.Random;

/**
 * Player modeling a computer which takes randomly a choice
 */
public class PlayerComputer extends PlayerAbs {

    public PlayerComputer (String _name) { name = _name;}
    @Override
    public void play(HashMap<String, GameComponent> _components) {

        Object[] choices = _components.keySet().toArray();
        Random r = new Random();
        componentPlayed = _components.get( choices[r.nextInt(choices.length)]);
    }


}
