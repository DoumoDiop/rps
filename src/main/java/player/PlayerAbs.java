package player;


import model.gameComponents.GameComponent;

import java.util.HashMap;

public abstract class PlayerAbs {

    protected String name;
    protected GameComponent componentPlayed;


    protected boolean winner = false;

    public boolean isWinner() {
        return winner;
    }

    public GameComponent getComponentPlayed() {
        return componentPlayed;
    }

    public void setWinner(boolean _winner) {
        winner = _winner;
    }

    @Override
    public String toString() {
        return name;
    }

    /**
     * Choosing what you will show to the opponent player
     *
     * @param _components
     */
    public abstract void play(HashMap<String, GameComponent> _components);

    /**
     * Player show what he chose for the round
     */
    public void showPlayed() {
        System.out.println(name + " played " + componentPlayed.toString());
    }

    ;
}
