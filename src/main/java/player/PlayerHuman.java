package player;

import model.gameComponents.GameComponent;

import java.util.HashMap;
import java.util.Scanner;

public class PlayerHuman extends PlayerAbs {
    private Scanner scanner;

    public PlayerHuman(String _name) {

         name = _name;
         scanner = new Scanner(System.in);
    }

    @Override
    public void play(HashMap<String, GameComponent> _components) {

        System.out.println("Go Ahead !!! ");
        String choice = "";

        while (!choice.equals("R") && !choice.equals("P") && !choice.equals("S")) {
            System.out.println("Type R for Rock , P for Paper and S for Scissor");
            choice = scanner.nextLine();

        }

        componentPlayed = _components.get(choice);
    }
}
