package factory.gameFactory;

import exception.UnAppropriateDominatorException;
import game.Game;
import player.PlayerAbs;

public class GameFactory {

    public static Game createGame(PlayerAbs p1, PlayerAbs p2) throws UnAppropriateDominatorException {

        return new Game(p1,p2);

    }
}
