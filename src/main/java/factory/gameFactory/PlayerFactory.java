package factory.gameFactory;

import player.PlayerAbs;
import player.PlayerComputer;
import player.PlayerHuman;

public class PlayerFactory {

    public static PlayerAbs createPlayerHuman(String name) {
        return new PlayerHuman(name);
    }

    public static PlayerAbs createPlayerComputer(String name) {

        return new PlayerComputer(name);

    }
}
