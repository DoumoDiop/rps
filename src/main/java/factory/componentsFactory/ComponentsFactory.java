package factory.componentsFactory;

import model.gameComponents.GameComponent;
import model.gameComponents.Paper;
import model.gameComponents.Rock;
import model.gameComponents.Scissor;

/**
 * Class creating components we need
 */
public class ComponentsFactory {
    private static GameComponent scissor, paper, rock;

    public static GameComponent createRock() {
        if (rock == null) rock = new Rock();
        return rock;
    }

    public static GameComponent createPaper() {
        if (paper == null) paper = new Paper();
        return paper;
    }

    public static GameComponent createScissor() {
        if (scissor == null) scissor = new Scissor();
        return scissor;
    }


}
