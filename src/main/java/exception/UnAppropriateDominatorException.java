package exception;

import model.gameComponents.GameComponent;

public class UnAppropriateDominatorException extends  Exception {

    /**
     *
     * @param dominated The component we want to assign dominator
     * @param potentialDominant component which pretend to be dominator of the first component
     */
    public UnAppropriateDominatorException(GameComponent dominated, GameComponent potentialDominant){
        super(potentialDominant.getClass().getSimpleName()+ " can't be dominator of " + dominated.getClass().getSimpleName());
    }
}
