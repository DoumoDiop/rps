package player;

import factory.gameFactory.GameFactory;
import factory.gameFactory.PlayerFactory;
import game.Game;
import org.junit.Before;
import org.junit.Test;

import static org.junit.Assert.*;

public class PlayerTest {

    Game game ;

    @Before
    public void setUp() throws Exception {
        game = GameFactory.createGame(PlayerFactory.createPlayerComputer("Computer 1"), PlayerFactory.createPlayerComputer("Computer 2"));

    }

    @Test
    public void beforeFirstPlay() throws Exception {
        assertEquals(null, game.getP1().componentPlayed);
    }

    @Test
    public void play() throws Exception {

        game.getP1().play(game.getGameComponents());

        assertNotEquals(null,game.getP1().componentPlayed);
    }

}