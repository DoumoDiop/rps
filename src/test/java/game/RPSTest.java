package game;

import factory.componentsFactory.ComponentsFactory;
import model.gameComponents.GameComponent;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import static org.junit.Assert.*;

public class RPSTest {

    GameComponent rock, paper, scissor;

    @Before
    public void setUp() throws Exception {
        rock = ComponentsFactory.createRock();
        paper = ComponentsFactory.createPaper();
        scissor = ComponentsFactory.createScissor();

        rock.setDominator(paper);
        scissor.setDominator(rock);
        paper.setDominator(scissor);

    }


    @Test
    public void testNonReflexivity() throws Exception {
        assertEquals(0,rock.compareTo(rock));
    }

    @Test
    public void testDomination() throws Exception {

        assertEquals(1,paper.compareTo(rock));
    }

}